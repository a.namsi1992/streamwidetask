package com.namsi.contact_system_datasource

import android.annotation.SuppressLint
import android.content.Context
import android.provider.ContactsContract
import com.namsi.contact_domain.Contact

class ContactsDeviceRepository(private val context: Context) {
    @SuppressLint("Range")
    suspend fun getContacts(): Map<String, Contact> =
        context.contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        ).use { phones ->
            val contactsMap = mutableMapOf<String, Contact>()
            if (phones != null) {
                while (phones.moveToNext()) {
                    val contactID =
                        phones.getString(phones.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID))
                    //contact phone
                    val phoneNumber: String =
                        phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            .replace(Regex("[-() ]"), "")
                    //contact name
                    val name: String = phones.getString(
                        phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))!!
                    //contact image if available
                    val imageUri = phones.getString(
                        phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

                    contactsMap[contactID] = Contact(contactID, name, imageUri, phoneNumber)
                }
            }
            contactsMap
        }
}