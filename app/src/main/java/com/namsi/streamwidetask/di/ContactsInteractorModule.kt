package com.namsi.streamwidetask.di

import android.content.Context
import com.namsi.contact_interactor.ContactInteractor
import com.namsi.contact_interactor.GetContacts
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object ContactsInteractorModule {

    @Provides
    @ViewModelScoped
    fun provideContactInteractor(@ApplicationContext appContext: Context): ContactInteractor =
        ContactInteractor.build(appContext)

    @Provides
    @ViewModelScoped
    fun provideGetContacts(contactInteractor: ContactInteractor): GetContacts = contactInteractor.getContacts

}