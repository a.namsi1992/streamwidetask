package com.namsi.streamwidetask.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.namsi.contact_domain.Contact
import com.namsi.streamwidetask.databinding.ContactItemBinding

class ContactsAdapter(private val contactsList: List<Contact>) :
    RecyclerView.Adapter<ContactsListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsListViewHolder {
        val itemBinding = ContactItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ContactsListViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ContactsListViewHolder, position: Int) {
        holder.bind(contactsList[position])
    }

    override fun getItemCount(): Int = contactsList.size
}