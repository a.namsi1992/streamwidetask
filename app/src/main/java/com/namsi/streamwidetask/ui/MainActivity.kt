package com.namsi.streamwidetask.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.namsi.contact_domain.Contact
import com.namsi.core.DataState
import com.namsi.streamwidetask.R
import com.namsi.streamwidetask.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private val contactViewModel: ContactViewModel by viewModels()
    private val contactsList = ArrayList<Contact>()
    private val contactsAdapter = ContactsAdapter(contactsList)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.contactRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.contactRecyclerView.adapter = contactsAdapter

        initObservables()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initObservables() {
        lifecycleScope.launchWhenResumed {
            contactViewModel.contactsListStateFlow.collect {
                when (it) {
                    is DataState.Error -> {
                        binding.permissionText.visibility = View.GONE
                        binding.progressBar.visibility = View.GONE
                        binding.contactRecyclerView.visibility = View.GONE
                        if (it.exception is SecurityException) {
                            checkPermission(Companion.CONTACT_PERMISSION_CODE)
                        }
                    }
                    DataState.Loading -> {
                        binding.permissionText.visibility = View.GONE
                        binding.contactRecyclerView.visibility = View.GONE
                        binding.progressBar.visibility = View.VISIBLE
                    }
                    is DataState.Success -> {
                        binding.permissionText.visibility = View.GONE
                        binding.progressBar.visibility = View.GONE
                        binding.contactRecyclerView.visibility = View.VISIBLE
                        contactsList.clear()
                        it.data?.let { it1 -> contactsList.addAll(it1) }
                        contactsAdapter.notifyDataSetChanged()
                    }
                }
            }
        }

    }

    private fun checkPermission(requestCode: Int) {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), requestCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Companion.CONTACT_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                contactViewModel.getContacts()
            }
            else {
                Toast.makeText(this, "Contacts Permission Denied", Toast.LENGTH_SHORT) .show()
                binding.permissionText.visibility = View.VISIBLE
                binding.progressBar.visibility = View.GONE
                binding.contactRecyclerView.visibility = View.GONE
            }
        }
    }

    companion object {
        const val CONTACT_PERMISSION_CODE = 1
    }
}