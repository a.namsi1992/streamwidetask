package com.namsi.streamwidetask.ui

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import com.namsi.contact_domain.Contact
import com.namsi.streamwidetask.databinding.ContactItemBinding

class ContactsListViewHolder(private val contactItemBinding: ContactItemBinding) :
    RecyclerView.ViewHolder(contactItemBinding.root) {
    @SuppressLint("SetTextI18n")
    fun bind(contact: Contact) {
        contactItemBinding.contactName.text = "${contact.name}"
        contactItemBinding.contactNumber.text = "${contact.phoneNumber}"
    }
}