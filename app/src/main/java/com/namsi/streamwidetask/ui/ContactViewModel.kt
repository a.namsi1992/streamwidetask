package com.namsi.streamwidetask.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.namsi.contact_domain.Contact
import com.namsi.contact_interactor.GetContacts
import com.namsi.core.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ContactViewModel @Inject constructor(private val getContacts: GetContacts) : ViewModel() {

    private val _contactsListStateFlow = MutableStateFlow<DataState<List<Contact>>>(DataState.Success(ArrayList()))
    val contactsListStateFlow = _contactsListStateFlow

    init {
        getContacts()
    }

    fun getContacts() {
        getContacts.execute().onEach {
            contactsListStateFlow.emit(it)
        }.launchIn(viewModelScope)
    }

}