package com.namsi.contact_interactor

import android.content.Context
import com.namsi.contact_system_datasource.ContactsDeviceRepository

class ContactInteractor(
    val getContacts: GetContacts
) {
    companion object{
        fun build(context: Context) : ContactInteractor =
            ContactInteractor(
                getContacts = GetContacts(
                    contactsDeviceRepository = ContactsDeviceRepository(context)
                )
            )
    }
}