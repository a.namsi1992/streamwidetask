package com.namsi.contact_interactor

import com.namsi.contact_domain.Contact
import com.namsi.contact_system_datasource.ContactsDeviceRepository
import com.namsi.core.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetContacts(
    private val contactsDeviceRepository: ContactsDeviceRepository,
) {
    fun execute(): Flow<DataState<List<Contact>>> = flow {
        emit(DataState.Loading)
        try {
            val contactsMap = contactsDeviceRepository.getContacts()
            emit(DataState.Success(contactsMap.values.toList()))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}