package com.namsi.contact_domain

data class Contact (
    val id: String,
    val name: String?,
    val imageUri: String?,
    val phoneNumber: String?
)